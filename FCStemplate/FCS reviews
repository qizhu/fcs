Frontiers of Computer Science



	 	
Decision Letter (FCS-15468)
From:
fcs@pub.hep.cn
To:
qizhu_cs@163.com
CC:
Subject:
Frontiers of Computer Science - Decision on Manuscript ID FCS-15468
Body:
04-Jan-2016 


Dear Mr. Zhu : 

Manuscript ID FCS-15468 entitled "Understanding Co-run Performance on CPU-GPU Integrated Processors: Observations, Insights, Directions" which you submitted to the Frontiers of Computer Science, has been reviewed. The comments of the reviewer(s) are included at the bottom of this letter. 

These reviews, all by recognized experts in the field, have been prepared with care. The reviews agree that this is an interesting paper that makes a substantial contribution. However, there are several concerns that have been brought up by the reviewers. On the basis of these reviews and my own reading of the paper, we are pleased to accept the paper for publication in Frontiers of Computer Science, conditional on receiving a revision that addresses the concerns identified in the reviews. 

We ask that you also include comments responding to each of the reviewers, with the reviewer identified by number, indicating how you have addressed each of the points raised in the review. It is important that the revised paper be as solid as possible, address in all major concerns, and addressing in a satisfactory way the remaining, smaller concerns. 

When submitting your revised manuscript, you will be able to respond to the comments made by the reviewer(s) in the space provided. You can use this space to document for any changes you make to the original manuscript. In order to speed up the processing of the revised manuscript, please be as specific as possible in your response to the reviewer(s). Failing to provide the response may lead to a direct reject. 

To revise your manuscript, please login to http://mc.manuscriptcentral.com/hepfcs and enter your Author Center, where you will find your manuscript title listed under "Manuscripts with Decisions". Under "Actions", click on "Create a Revision". Your manuscript number has been appended to denote a revision. 

Once the revised manuscript is prepared, you can upload it and submit it through your Author Center. Besides, please upload the response togeter with the revised manuscript as two main documents, thus the system wll creat a pdf with both of the parts. 

IMPORTANT: Your original files are available to you when you upload your revised manuscript. Please delete any redundant files before completing the submission. 
Failing to provide the response may lead to a direct reject. 

We are trying to minimize the time to publication. This requires help from both reviewers and authors. Please ensure that we receive your revision within **ONE** month. If it is not possible for you to submit your revision by the deadline, we may have to consider your paper as a new submission. 

Thank you for submitting your manuscript to the Frontiers of Computer Science! We look forward to receiving your revision. 

Sincerely, 
Frontiers of Computer Science 

*************************************************** 
Frontiers of Computer Science Editorial Office 
E-mail: fcs@pub.hep.cn 
Tel: +86 (0)10 8233 8176 
http://www.springer.com/computer/journal/11704 
http://journal.hep.com.cn/fcs 
http://mc.manuscriptcentral.com/hepfcs 
*************************************************** 

Reviewer(s)' Comments to Author: 

Reviewer: 1 

Comments to the Author 
This paper gives a thorough analysis of the co-run programs on CPU and GPU which are integrated on one chip. It tests on different hardware platforms, considers different OS, and explains the results soundly. There are several new and interesting findings, and the paper also tries different methods to reduce the side effect of co-runs. I think the findings in the paper will be very helpful to readers. 

One question is that the paper says the GPU kernel time isn't affected much by the memory access behaviour of the CPU contender, however it didn't explain the reason. Is it because the cache is partitioned for GPU and CPU? Another question is that the paper proposes to let GPU do data copy instead of CPU, but i didn’t see the overhead analysis of doing that in the paper. 

Reviewer: 2 

Comments to the Author 
The integration of CPU and GPU has posed significant challenges to researchers. This work investigates the performance implications of independently co-running CPU and GPU programs. 

This paper is generally well organized and clearly written, it is an extension of the previous paper published in LCPC 2014. The contributions of this paper include following aspects: 
1)	The authors perform a comprehensive measurement that covers a wide variety of factors, including processor architectures, operating systems, benchmarks, timing mechanisms, inputs, and power management schemes. 
2)	This paper analyzes these observations and clarify the important roles of OS context switching and power management in determining the program performance, and the subtle eﬀect of CPU-GPU data copying. 
3)	The authors evaluate some case studies, and point out some promising directions to mitigate anomalous performance degradation on integrated heterogeneous processors. 

I have following comments on this paper which can be considered in the revision. 
1)	As an extension of the conference paper, I suggest the authors to clearly mention the extensions based on the previous article. The authors have significantly Section 3 and 4, while the other sections are pretty the same to the conference paper. 
2)	Beside the figures showing the experimental results, it is also favorable to illustrate the system model and design flow with comprehensive figures. 
3)	The z-axis of Fig .7 is not proper to show the results, please adjust. The same to Fig. 9, Fig. 12. Also the fonts of the legends in most figures should be increased. 
4)	The authors should make a deeper and comprehensive analysis of the related work. The conference version has more references than the journal version, so you may need also add some more references to the current manuscript. 

Based on the above comments, I suggest the authors to make a revision. 


Reviewer: 3 

Comments to the Author 
The paper tackles the problem of co-run performance in GPU-CPU systems. This paper is well motivated and the overall quality is ok, however, I still have the following comments: 
1. Architecture details on your experimental platforms. The authors are expected to put some backgrounds on your experimental platforms. This paper is full of experimental results, and lacks the analysis on the fundamental reasons behind them. I feel that the architecture details are needed to make your paper more convincing. For example, you may want to list (if you can) details about cache, bandwidth, memory polices and other architecture details, because they may impact the overall system performance significantly, and also play an important role in analyzing the co-run cases. 

2. Application’s memory behaviors. You are expected to show the memory patterns of your benchmarks. As known to us that, besides the architecture details, the overall system performance is also sensitive to application’s features. Moreover, studied in many efforts [Y.Kim Micro-2010, Lei Liu ISCA-2014], memory characteristic plays a very important role in co-run cases, where performance is impacted by diversity. Thus, pls do not let memory features in blind, and put some analysis between the co-run performance and this. 

3. The image quality of all of figures is lower. The words, labels are hard to read. I do not think the 3-d graphs are better than 2-d ones to show the information in your work. 

4. Some important studies in reducing co-run contentions on a multicore system are missed in your related work. Such as Lei Liu’s work in ISCA-2014, J. Lin’s efforts in HPCA-2018, etc. I think a good survey will make your paper better. 

All in all, I think this work needs further efforts. I’ll happily review a revision of this paper.
Date Sent:
04-Jan-2016
 


© Thomson Reuters |  © ScholarOne, Inc., 2015. All Rights Reserved.
