colormap copper;

subplot(3,3,4);
set(gca, 'FontSize', 9);
% 0 0.1mSec 1mSec 10mSecs
linux   = [118 151 231 230];
poll    = [114 114 115 116];
wakecpu = [108 118 157 161];
Y = linux' * [1 0 0] + poll' * [0 1 0] + wakecpu' * [0 0 1];
h = bar(Y, 0.6, 'group');
legend(h, 'Linux', 'Disable CPU sleeps', 'Anticipatory wakeup', 'Location', 'NorthOutside');
set(legend, 'Orientation','horizontal', 'Position',[0.35 0.69 0.01 0.01]);
axis([0.4 4.6 0 250]);
set(gca, 'XTick', 1:1:4);
set(gca, 'XTickLabel', {'0', '100', '1000', '10000'});
xlabel('Inter-I/O idle time (uSecs)');
ylabel('99th %tile latency (uSecs)');
box off;
title('I/O latency');

subplot(3,3,5);
set(gca, 'FontSize', 9);
% 0 0.1mSec 1mSec 10mSecs
x       = [1   2   3   4];
linux   = [49.940  29.211  25.302  15.972];
poll    = [93.970  93.330  93.579  87.598];
wakecpu = [49.888  29.523  25.133  16.339];
Y = linux' * [1 0 0] + poll' * [0 1 0] + wakecpu' * [0 0 1];
h = bar(Y, 0.6, 'group');
axis([0.4 4.6 0 100]);
set(gca, 'XTick', 1:1:4);
set(gca, 'XTickLabel', {'0', '100', '1000', '10000'});
set(gca, 'YTick', 0:20:100);
xlabel('Inter-I/O idle time (uSecs)');
ylabel('CPU+DRAM Power (Watts)');
box off;
title('Power');

fname = 'ssd_perf_power';
print('-depsc', fname);
cmd = sprintf('gs -q -sDEVICE=pdfwrite -dEPSCrop -sOutputFile=%s.pdf -dBATCH -dNOPAUSE %s.eps', fname, fname);
system(cmd);
