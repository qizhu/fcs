#!/bin/bash

for f in $(ls *.pdf); do
  inkscape $f --export-eps=${f%.*}.eps
done
