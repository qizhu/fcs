colormap copper;

subplot(3,3,4);
set(gca, 'FontSize', 9);
power = [14.042 50.576 58.639 63.795 69.829 73.869 76.564 79.194 81.741 82.579 84.917 87.082 88.694 88.772 88.907 89.170 89.980 90.940 90.744 91.326 91.274 91.335 91.624 91.836 92.515];
h = bar(power, 0.4);
ch = get(h,'Children');
fvd = get(ch,'Faces');
fvcd = get(ch,'FaceVertexCData');
fvcd(fvd(1,:)) = 2;
for i = 2:length(power)
    fvcd(fvd(i,:)) = 1;
end
set(ch,'FaceVertexCData',fvcd);
axis([0 26 0 100.0]);
set(gca, 'XTick', [1 7 13 19 25]);
set(gca, 'XTickLabel', {'0', '6', '12', '18', '24'});
xlabel('Number of active CPUs');
ylabel('CPU+DRAM power (Watts)');
box off;
title('(A) Haswell-based server');

subplot(3,3,5);
set(gca, 'FontSize', 9);
power = [0.50 0.74 0.86 1.01 1.19 2.29 2.88 3.54 4.14];
h = bar(power, 0.4);
ch = get(h,'Children');
fvd = get(ch,'Faces');
fvcd = get(ch,'FaceVertexCData');
fvcd(fvd(1,:)) = 2;
fvcd(fvd(2,:)) = 2;
fvcd(fvd(3,:)) = 2;
fvcd(fvd(4,:)) = 2;
fvcd(fvd(5,:)) = 2;
fvcd(fvd(6,:)) = 1;
fvcd(fvd(7,:)) = 1;
fvcd(fvd(8,:)) = 1;
fvcd(fvd(9,:)) = 1;
set(ch,'FaceVertexCData',fvcd);
axis([0 10 0 4.2]);
set(gca, 'XTick', [1 5 9]);
set(gca, 'XTickLabel', {'0', '4', '8'});
xlabel('Number of active CPUs');
ylabel('Smartphone power (Watts)');
box off;
title('(B) Exynos-based smartphone');

%subplot(3,2,2);
%set(gca, 'FontSize', 7);
%power = [4.7 40.9 45.7 48.3 50.5 53.0 55.3 58.0 60.7 63.3 65.7 68.7 71.1 73.9 76.5 79.3 81.7 84.6 87.3 90.1 92.8 93.3 93.5 93.6 94.0 94.4 94.5 94.8 95.5 95.4 95.9 96.0 96.2 96.3 96.6 96.9 97.1 97.2 97.3 97.8 98.0];
%h = bar(power, 0.4);
%colormap copper;
%ch = get(h,'Children');
%fvd = get(ch,'Faces');
%fvcd = get(ch,'FaceVertexCData');
%for i = 1:length(power)
%    fvcd(fvd(i,:)) = 1;
%end
%fvcd(fvd(1,:)) = 2;
%set(ch,'FaceVertexCData',fvcd);
%axis([0 42 0 100.0]);
%set(gca, 'XTick', [1 2 11 21 31 41]);
%set(gca, 'XTickLabel', {'0', '1', '10', '20', '30', '40'});
%xlabel('Number of active CPUs');
%ylabel('Package power (Watts)');
%box off;
%title('Dual-socket IvyBridge power');

%subplot(3,3,3);
%set(gca, 'FontSize', 7);
%power = [0.34 1.87 2.20 2.55 2.84];
%h = bar(power, 0.4);
%colormap copper;
%ch = get(h,'Children');
%fvd = get(ch,'Faces');
%fvcd = get(ch,'FaceVertexCData');
%for i = 1:length(power)
%    fvcd(fvd(i,:)) = 1;
%end
%fvcd(fvd(1,:)) = 2;
%set(ch,'FaceVertexCData',fvcd);
%axis([0 6 0 3.0]);
%set(gca, 'XTick', 1:1:5);
%set(gca, 'XTickLabel', 0:1:4);
%xlabel('Number of active CPUs');
%ylabel('Power (Watts)');
%box off;
%title('Nexus 7 tablet power');

fname = 'multicore_power';
print('-depsc', fname);
cmd = sprintf('gs -q -sDEVICE=pdfwrite -dEPSCrop -sOutputFile=%s.pdf -dBATCH -dNOPAUSE %s.eps', fname, fname);
system(cmd);
