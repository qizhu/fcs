subplot(3,2,1);
set(gca, 'FontSize', 8);
power = [4.7 40.9 45.7 48.3 50.5 53.0 55.3 58.0 60.7 63.3 65.7 68.7 71.1 73.9 76.5 79.3 81.7 84.6 87.3 90.1 92.8 93.3 93.5 93.6 94.0 94.4 94.5 94.8 95.5 95.4 95.9 96.0 96.2 96.3 96.6 96.9 97.1 97.2 97.3 97.8 98.0];
h = bar(power, 0.4);
colormap copper;
ch = get(h,'Children');
fvd = get(ch,'Faces');
fvcd = get(ch,'FaceVertexCData');
for i = 1:length(power)
    fvcd(fvd(i,:)) = 1;
end
fvcd(fvd(1,:)) = 2;
set(ch,'FaceVertexCData',fvcd);
axis([0 42 0 100.0]);
set(gca, 'XTick', [1 2 11 21 31 41]);
set(gca, 'XTickLabel', {'0', '1', '10', '20', '30', '40'});
xlabel('Number of active CPUs');
ylabel('Package power (in Watts)');
box off;
%title('Dual-socket IvyBridge power');

%subplot(3,3,2);
%set(gca, 'FontSize', 8);
%power = [0.34 1.87 2.20 2.55 2.84];
%h = bar(power, 0.4);
%colormap copper;
%ch = get(h,'Children');
%fvd = get(ch,'Faces');
%fvcd = get(ch,'FaceVertexCData');
%for i = 1:length(power)
%    fvcd(fvd(i,:)) = 1;
%end
%fvcd(fvd(1,:)) = 2;
%set(ch,'FaceVertexCData',fvcd);
%axis([0 6 0 3.0]);
%set(gca, 'XTick', 1:1:5);
%set(gca, 'XTickLabel', 0:1:4);
%xlabel('Number of active CPUs');
%ylabel('Power (in Watts)');
%box off;
%title('Nexus 7 tablet power');

fname = 'multicore_power';
print('-depsc', fname);
cmd = sprintf('gs -q -sDEVICE=pdfwrite -dEPSCrop -sOutputFile=%s.pdf -dBATCH -dNOPAUSE %s.eps', fname, fname);
system(cmd);
