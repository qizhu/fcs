\BOOKMARK [1][-]{section.1}{Introduction}{}% 1
\BOOKMARK [1][-]{section.2}{GPU Control Dilemma}{}% 2
\BOOKMARK [2][-]{subsection.2.1}{GPU and Host Control Program}{section.2}% 3
\BOOKMARK [2][-]{subsection.2.2}{Tensions Among Responsiveness, Energy Efficiency, and Interference}{section.2}% 4
\BOOKMARK [1][-]{section.3}{Enabling Timely GPU Control}{}% 5
\BOOKMARK [2][-]{subsection.3.1}{Basic Anticipatory Wakeup \(BAW\)}{section.3}% 6
\BOOKMARK [2][-]{subsection.3.2}{Three-stage Anticipatory Wakeup \(TAW\)}{section.3}% 7
\BOOKMARK [3][-]{subsubsection.3.2.1}{Model-Based Auto-Configuration}{subsection.3.2}% 8
\BOOKMARK [3][-]{subsubsection.3.2.2}{Self Correction to Reduce Error Impact}{subsection.3.2}% 9
\BOOKMARK [2][-]{subsection.3.3}{Splitting-based Anticipatory Wakeup \(SAW\)}{section.3}% 10
\BOOKMARK [1][-]{section.4}{Implementation through Source-to-Source Compiler}{}% 11
\BOOKMARK [1][-]{section.5}{Evaluation}{}% 12
\BOOKMARK [2][-]{subsection.5.1}{Experiment Setup}{section.5}% 13
\BOOKMARK [3][-]{subsubsection.5.1.1}{Hardware and Software Platforms}{subsection.5.1}% 14
\BOOKMARK [3][-]{subsubsection.5.1.2}{Metrics and Measurement Methods}{subsection.5.1}% 15
\BOOKMARK [2][-]{subsection.5.2}{BAW and TAW}{section.5}% 16
\BOOKMARK [3][-]{subsubsection.5.2.1}{Performance Benefits}{subsection.5.2}% 17
\BOOKMARK [3][-]{subsubsection.5.2.2}{Power Benefits}{subsection.5.2}% 18
\BOOKMARK [3][-]{subsubsection.5.2.3}{Putting Together: Benefits of EDP}{subsection.5.2}% 19
\BOOKMARK [2][-]{subsection.5.3}{SAW and Auto-selection}{section.5}% 20
\BOOKMARK [2][-]{subsection.5.4}{Influence to co-run programs}{section.5}% 21
\BOOKMARK [1][-]{section.6}{Related Work}{}% 22
\BOOKMARK [1][-]{section.7}{Conclusion}{}% 23
\BOOKMARK [1][-]{section.8}{References}{}% 24
