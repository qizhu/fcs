\section{Evaluation}
\label{sec:eval}

% In this section, we first introduce the setup of our experiment setup,
% including details of the specific CPU\&GPU fusion processor, configurations
% of OS and driver, metrics, and metrics measurement. Then the benefits of
% variants of BAW and SAW are illustrated on performance, power, and power
% efficiency. At the end of this section, the best variant is
% selected to deploy in GPU program. Then we measure the performance
% when programs are co-running on CPU and GPU. The degradation of the co-run CPU
% program is measured to show how these synchronize methods of GPU program can
% affect the co-run performance.

\subsection{Experiment Setup}
This section describes the hardware and software platforms used in our study.

\subsubsection{Hardware and Software Platforms}
\paragraph{Hardware Platform}
% In this work, we employ the CPU \& GPU fused architecture---AMD APU (Richland)
% which contains several CPU cores and one integrated GPU on a single chip. The
% CPU and the GPU share memory on this platform, but there is no shared last
% level cache between CPU and GPU.
%
% Specifically, as a member of AMD APU family, A10 6800k includes four 4.1GHz
% CPU cores, 4MB L2 cache, and 384 Radeon (shader) cores clocked at the peak
% level of 844MHz. A10 6800k has two level CPU sleep states, named as S1 and S2.
% According to our measurement, the wakeup overhead of S1 is about 30
% microseconds, and that of S2 is about 100 microseconds.

We focus our study on an AMD fused processor, A10 6800K, which consists of four 4.1 GHz
CPU cores, 4MB L2 cache, and 384 Radeon shader cores clocked at the peak level of 844 MHz.
The CPU and the GPU cores share the same main memory but not the last level cache. The CPU
has two level sleep states, named as S1 and S2. According to our measurement, the wakeup
overhead of S1 is about 30 $\mu$s, and that of S2 is about 100 $\mu$s.

\paragraph{OS, Driver and Benchmark}

We use the driver from AMD named Catalyst to manipulate CPU and GPU in the fused
processor. We use OpenCL version 1.2 for both CPU and GPU runs, and the Ubuntu operating
system (kernel version 3.13).

We select benchmarks from AMD APP SDK~\cite{amdappsdk} and Rodinia~\cite{rodinia}. These
benchmark suits contain a set of carefully written OpenCL programs, which can be executed
on both CPU and GPU. The selected benchmarks cover a broad range of domains, including 
multimedia processing, fluid phsics, financial analysis, graph searching, and so on. 
Among those benchmarks, {\em BFS}, {\em CFD} and {\em LUD}, which are from Rodinia suit,
are irregular in terms of memory access pattern, variance of the inputs across invocations,
or the level of parallelism. The rest ones are regular benchmarks.

\subsubsection{Metrics and Measurement Methods}
We consider four metrics, namely performance, power, hit rate and EDP to quantify
different aspects of the designs.

\paragraph{Responsiveness} It represents the kernel execution time observed by the GPU host
thread, comprising three parts: kernel launch overhead, kernel execution time on device,
and CPU wakeup overhead. The optimizations we develop are to reduce or eliminate the third
part, wakeup overhead. To alleviate system noise, we repeat each experiment at least 200 
times and report the average.

% Wall-clock time is measured to show the performance of GPU kernel. It is the
% human perception of the passage of time from the start to the completion of
% a task. In this work, we don't try to speed up GPU kernel as many work has
% been done in the GPGPU field. We try to alleviate the CPU \& GPU interaction,
% so only wall-clock time can show this benefits.
%
% Because of some system noise, the fluctuation of kernel running time exists
% in our experiment and the influence is so significant that it can't be ignored.
% Figure~\ref{fig:fluctuation} gives one example of the running time
% distribution when kernel is executed repeatedly and the fluctuation is about
% 20\% of the running time. Due to that, we can't ensure that every
% kernel completion can be caught in
% the busy-polling stage. It affects the predicting accuracy so that we can
% never get the best performance with a tight power constraint. Considering the
% influence of fluctuation, average time is used to show the kernel running
% time.

\paragraph{Power}
Power efficiency is a critical metric to consider in this work. We use Wattsup
pro~\cite{wattsup}, a power meter, to measure the power of the whole system in watts for
different kernel completion detection approaches. With the help of the repeated
executions, the power meter can provides accurate measurement.

\paragraph{Predicting accuracy} It is highly important for the effectiveness of BAW, TAW,
and SAW. As discussed in Section~\ref{sec:aw}, perfect prediction accuracy enables
complete elimination of the wakeup overhead. Based on the model introduced in
section~\ref{sec:aw}, we observe the hit of a prediction when the kernel completion
is caught in the Busy-polling stage.

\paragraph{EDP} EDP serves as a metric to consider both responsiveness and power.
According to the previous work~\cite{gonzalez1996energy}, we define EDP as (T * T * W),
where T represents execution time, and W represents power consumption. A lower EDP value
indicates better power efficiency. Our purpose is to strike a good trade-off between
performance and power to provide a better solution for power efficiency.

\paragraph{Baseline Schemes}
As mentioned before, Interrupt and Busy-polling are two basic and popular
methods to do synchronization. Previous work on co-scheduling and the
polling vs. interrupt based communication in clusters have revealed that they
both have advantage and disadvantage. It is hence hard to claim which is better.
In this work, we treat them as two baseline
schemes.
% In the next subsections, it will be shown that our methods can
% outperform them on either performance or power, and our methods can achieve
% better power efficiency than both of them.

\subsection{BAW and TAW}
In this subsection, we evaluate the previously described four metrics for BAW and two
variants of TAW, namely TAW\_model and TAW\_level. We compare them with the traditional
methods, Interrupt and Busy-polling, to show their effectiveness in achieving a better
trade-off between performance and power efficiency.

\subsubsection{Performance Benefits}

\begin{figure}[t]
\centering
\includegraphics[width=\columnwidth]{figures/Evaluation/fig_speedup_BAW.eps}
  \vspace*{-1em}
\caption{The speedup over Interrupt.}\label{fig:speedup_BAW}
\vspace*{-1em}
\end{figure}

Figure~\ref{fig:speedup_BAW} shows the performance improvement over Interrupt
method from Busy-polling, BAW and TAW's variants. Overall, all benchmarks
show speedup for BAW, TAW\_model, and TAW\_level, which demonstrates that
our method can improve the responsiveness of host thread. However, none of our methods
beats Busy-polling. Busy-polling completely eliminates the wakeup overhead, but our methods
cannot achieve that due to imperfect predictions. Even for regular benchmarks, which are
relatively easy to predict, the system noise causes non-trivial kernel execution time
fluctuation across kernel invocations. For example, Figure~\ref{fig:fluctuation} shows the
kernel execution time of different invocations with the same input, demonstrating a
fluctuation range of around 20\%.

\begin{figure}[t]
\centering
\includegraphics[width=\columnwidth]{figures/Evaluation/fig_fluctuation.eps}
  \vspace*{-1em}
\caption{Example of runtime fluctuation of repeated kernel
	execution.}\label{fig:fluctuation}
\vspace*{-1em}
\end{figure}
%

We observe that TAW\_level produces better speedups than the other two designs do. This is
due to the multi-level features of self-correction. In TAW\_level, Busy-polling is used in
each level interval. Because of the running time fluctuation, even for the regular
benchmarks, which should have only one level, multiple levels still exist. Therefore, the
Busy-polling period of TAW\_level is usually longer than the other variants, and the
chance that kernel completes in the Busy-polling stage is higher. Similarly, for irregular
benchmarks TAW\_level performs significantly better, yielding around two times extra
improvement over TAW\_model, because the kernel execution time is extremely hard to
predict. This is confirmed by the prediction accuracies of TAW\_model and TAS\_level, shown
in Figure~\ref{fig:hit_rate_BAW}. On average, TAW\_level's hit rate is on an average 37\%
better than TAW\_model's.

\begin{figure}[t]
\centering
\includegraphics[width=\columnwidth]{figures/Evaluation/fig_hit_rate_BAW.eps}
  \vspace*{-1em}
\caption{Example of predicting accuracy of TAW
	variants.}\label{fig:hit_rate_BAW}
\vspace*{-1em}
\end{figure}

\subsubsection{Power Benefits}

\begin{figure}[t]
\centering
\includegraphics[width=\columnwidth]{figures/Evaluation/fig_power_BAW.eps}
  \vspace*{-1em}
\caption{The power increase over Interrupt.}\label{fig:power_BAW}
\vspace*{-1em}
\end{figure}

Figure~\ref{fig:power_BAW} shows the power increase of busy polling, BAW, TAW\_model and
TAW\_level over Interrupt. Since Busy-polling keeps polling the state register of the GPU,
the CPU has to be active during the whole GPU kernel execution. As a result, Busy-polling
yields as much as 75\% power increase. Our designs, however, cause much less power
increase, as the CPU goes to sleep for a significant portion of the GPU kernel execution
time.

Among our designs, TAW\_level consumes more power because of the long Busy-polling period.
Recall that TAW\_level shows better responsiveness due to the same reason. In the {\em
FastWalshTransform} case, TAW\_level consumes as much power as Busy-polling does. This is
due to the extreme short execution time of the kernel, which is only 54 $\mu$s. In
this case, CPU does not go to sleep under TAW\_level because the lower bound of the level
is even lower than the threshold of S1 state.

Figure~\ref{fig:power_BAW} also confirms the advantage of TAW\_model's design to reduce
the sum of Busy-polling length. It can be seen that TAW\_model only brings 9\% average power increase, which
is significantly less than BAW (13\%) and TAW\_level(13\%).

\subsubsection{Putting Together: Benefits of EDP}
To consider both responsiveness and power, Figure~\ref{fig:EDP_BAW} shows the normalized
EDP of Busy-polling and our three designs over Interrupt. Lower bars indicate better power
efficiency. We observe that Busy-polling increases EDP by around 5\%. Although
Busy-polling produces the best performance, it is not enough to compensate the power
increase.  On average, BAW and the two TAW variants are better than both Interrupt and
Busy-polling.  TAW\_level performs better than the other two designs because the large
speedup it obtains outweighs the power price it pays.

\begin{figure}[t]
\centering
\includegraphics[width=\columnwidth]{figures/Evaluation/fig_EDP_BAW.eps}
  \vspace*{-1em}
\caption{The normalized EDP of Busy-polling and
	TAW variants.}\label{fig:EDP_BAW}
\vspace*{-1em}
\end{figure}

We, however, note three exceptions, {\em DwtHaar1D}, {\em FastWalshTranform} and {\em
Reduction}, for which none of our designs outperforms Interrupt. The reason is that the
running time of these kernels is too short, which is less than 70 $\mu$s. The
performance gain potential is so small that compared with Interrup, the reduced wakeup
overhead can not pay off the additional power increase.

% Besides the three exceptions, due to the running time fluctuation, several
% BAW variants do not show better power efficient than Busy-polling or
% Interrupt. However, these odd cases can not deny the overall efficiency of
% BAW variants.

\subsection{SAW and Auto-selection}
We now evaluate the other approach, SAW, to wake up CPU in an anticipatory way. Because
last subsection shows the advantage of self-correctness, due to space limit, we only
consider SAW\_level in this section as the representative of SAW variants family.

\paragraph{Performance issue of SAW}
We normalize the performance improvement of Busy-polling, TAW\_level and SAW\_level to
Interrupt. As shown in Figure~\ref{fig:speedup_BAW_SAW}, SAW\_level on average produces
only 1\% improvement. For several benchmarks (e.g., DCT and MatrixMulImage), SAW\_level
performs much worse than Interrupt does. The source of degradation is due to the kernel
splitting scheme. Under SAW\_level, if one of the sub-kernels does not have enough
parallelism, the GPU is not fully utilized, leading to performance loss.

In some cases, such as {\em CFD}, SAW\_level even outperforms Busy-polling. This
surprising result comes from the the accidental reduction of kernel launch time. The
overhead of kernel launch increases with the number of created threads.  Kernel splitting
partitions the entire overhead into two parts, each associated with a sub-kernel. Recall
that under SAW, sub-kernels are launched in asynchronously. The kernel launch overhead of
second sub-kernel is overlapped with the execution of the first sub-kernel, leading to
some performance improvement.

\begin{figure}[t]
\centering
\includegraphics[width=\columnwidth]{figures/Evaluation/fig_speedup_BAW_SAW.eps}
  \vspace*{-1em}
\caption{The performance improvement over Interrupt of Busy-polling,
        TAW\_level and SAW\_level.}\label{fig:speedup_BAW_SAW}
\vspace*{-1em}
\end{figure}

\paragraph{Power gain of SAW}

Figure~\ref{fig:power_BAW_SAW} shows the power increase of Busy\_polling, TAW\_level, and
SAW\_level over Interrupt. Just like TAW\_level, the power consumption of SAW\_level is
significantly less than Busy-polling thanks to the sleeping stage. However, we still observe
around 22\% power increase from SAW\_level over Interrupt, which is about 7\% more than
that of TAW\_level. This significant additional power increase is introduced by the kernel 
splitting scheme. As one kernel is divided into two parts, neither is long enough for the CPU to
enter a deeper sleep state, preventing the CPU from saving power. 

%SAW\_level provides some improvement over TAW\_level for the irregular benchmarks. The
%reason is that the predicted execution time may be much smaller ?? Qi, can you explain
%this?

\begin{figure}[t]
\centering
\includegraphics[width=\columnwidth]{figures/Evaluation/fig_power_BAW_SAW.eps}
  \vspace*{-1em}
\caption{The power increment over Interrupt of Busy-polling,
        BAW\_level and SAW\_level.}\label{fig:power_BAW_SAW}
\vspace*{-1em}
\end{figure}

\paragraph{Normalized EDP of SAW}

As expected, Figure~\ref{fig:EDP_BAW_SAW} shows that for most of the regular benchmarks,
SAW\_level performs worse than either Interrupt method or Busy-polling method, because of
the significant performance loss or power loss caused by the kernel splitting scheme.
{\em BlackScholes}, {\em Reduction}, {\em BFS} and {\em CFD} under SAW\_level have the
best power efficiency thanks to the reduced overhead of kernel launch.

\begin{figure}[t]
\centering
\includegraphics[width=\columnwidth]{figures/Evaluation/fig_EDP_BAW_SAW.eps}
  \vspace*{-1em}
\caption{The normalized EDP of Busy-polling, TAW\_level and
        SAW\_level.}\label{fig:EDP_BAW_SAW}
\vspace*{-1em}
\end{figure}

\paragraph{Automatic selection of the best design}
From the previous results, we observe that when the kernel execution time is long (enough
parallelism) and the execution time varies significantly across iterations (multiple
levels), SAW\_level performs better than TAW\linebreak[4]\_level. Based on this observation, we
experiment with an automatic selection approach which is based on the runtime autotuning.
Basically, we profile the kernel length measured by the device timer. 
Figure~\ref{fig:EDP_best} shows 11\% reduction of EDP compared with Interrupt,
demonstrating that the simple heuristic can accurately select the best of the two designs. 

\begin{figure}[t]
\centering
\includegraphics[width=\columnwidth]{figures/Evaluation/fig_EDP_best_variant.eps}
  \vspace*{-1em}
\caption{The normalized EDP of Busy-polling and best
	variant on power efficiency.}\label{fig:EDP_best}
\vspace*{-1em}
\end{figure}

\vspace*{.1in}
We also tried exponential backoff scheme. It wakes up the control
thread to poll the status of GPU periodically, with the period length
increases exponentially. It is a scheme used in many runtime
controls. It however does not suite GPU.  We tried a spectrum of
configurations of the initial period length and polling period length,
but found that the results are not as good as SAW: 5-10\% less
speedups and 5-20\% higher EDP. Detailed results are omitted for the
sake of space.

\subsection{Influence to co-run programs}

We now evaluate the influence of our designs to the co-run CPU programs.
Figure~\ref{fig:corun_variant} shows the results for Interrup, Busy-polling and the
automatically selected best design between TAW\_level and SAW\_level. Interrupt brings
the minimum interference, because the GPU control thread goes to sleep immediately after
the kernel invocation. Busy-polling causes the largest interference, as the polling thread
continuously competes with the CPU program. The selected design lies in between, which
tries to manage the balance between sleep and polling better.

\begin{figure}[t]
\centering
\includegraphics[width=\columnwidth]{figures/Evaluation/fig_corun_variant.eps}
  \vspace*{-1em}
\caption{The co-run interference degree of Interrupt, Busy-polling and
        our best variant.}\label{fig:corun_variant}
\vspace*{-1em}
\end{figure}  



