\section{GPU Control Dilemma}
\label{sec:back}

In this section, we first provide some background on how GPU works and
the role of the host control program, and then describe the fundamental
challenge facing GPU control: the tensions among control delay,
energy efficiency, and co-run interference.

\subsection{GPU and Host Control Program}\label{sec:back1}

As a massively parallel architecture, GPU features a large number of
cores. A CPU-GPU integrated processor has a multicore CPU and a GPU
fused into a single chip which, from the programmer's perspective,
share a single memory address space. The communication between CPU and
GPU becomes simple and fast. 

In current GPU execution models (e.g., OpenCL~\cite{OpenCL} and CUDA
\cite{CUDA}), there is typically a host control program, which runs
on CPU but is responsible for launching GPU kernels and reacting to
their terminations.  In this paper, we use
OpenCL as the programming model for its broad support across
architectures. 

In OpenCL, a GPU kernel launch typically follows the following
process. The control program calls an API to put the GPU kernel into a
command queue, and moves on (and possibly enqueues more GPU
kernels). Meanwhile, the GPU driver automatically dequeues the kernel
and launches it on GPU when GPU becomes available.  When there are
dependences between two kernel calls, a programmer can put a waiting
point (e.g., a call of {\em clFinish}) between the two calls such that
the control program waits at that point until it finds out the completion
of the previous kernel execution.  GPU control delay refers to the
time gap between the actual completion of a GPU kernel and the time when
the control program finds that out.

%% A GPU kernel launch can be synchronous, in which case, the control
%% program waits for the termination of the kernel before executing the
%% instructions that follow the kernel launch. It can also be asynchronous,
%% in which case, the control program continues its execution without
%% waiting for the completion of the kernel. It may still gets blocked
%% later. For example, in OpenCL programs, when control program reaches the 
%% interface {\em clFinish}, it is blocked until all previously queued OpenCL 
%% commands in command\_queue are issued to the associated device and 
%% have completed~\cite{clFinish}. 

GPU control delay closely relates with factors in the OS and GPU
drivers.  The first factor is the notification mechanism of the completion
of a GPU kernel. There are two basic mechanisms: \underline{hardware
  interrupt} and \underline{register polling}. In the former case, the
GPU driver sends a hardware interrupt signal to CPU and the control
program is notified through the interrupt handler. The GPU control
responsiveness is determined by the time needed by the interrupt
handling. In the latter case, the GPU driver sets a flag in a
memory-mapped register, and the control program finds out the completion of
the kernel by polling that register. The GPU control responsiveness is
determined by the polling frequency.

On AMD Richland GPU (Linux 3.13), for instance, both mechanisms are
supported. Interrupt \#59 is the hardware interrupt for that purpose,
and {\em clGetEventInfo} in OpenCL offers the interface for
programmers to implement their polling scheme. In AMD OpenCL SDK, a
function named {\em waitForEventAndRelease} is a representative
polling scheme, which wraps {\em clGetEventInfo} with a loop, keeping
polling for the status of the kernel. We call it {\em busy polling} or
{\em busy waiting}.

%% With the help of {\em ftrace} in Linux, we analysis the system call 
%% sequence of {\em clFinish} and {\em clWaitForEvents}, which are 
%% interfaces provided by AMD 
%% runtime system to synchronize OpenCL kernel. In single-run case, 
%% %after {\em clFinish} is invoked, the control program is switched out 
%% %by the idle task. The CPU core goes to sleep (being in C-state) to 
%% %save power. Sometime later, 
%% CPU core is waked up by the 59\# interrupt. The 59\# interrupt is 
%% labled as {\em fglrx[0]@PCI:0:1:0} in the interrupt table of
%% the experiment machine, which indicates that interrupt comes from 
%% OpenCL driver. So it is confirmed that in AMD OpenCL driver,
%% \underline{hardware interrupt} is used to implement kernel 
%% synchronization. 

%% Both OpenCL and CUDA provide kernel state detecting interfaces, such as
%% {\em clGetEventInfo} in OpenCL and {\em cudaStreamQuery}, 
%% {\em cudaEventQuery} in CUDA. With the help of these interfaces, 
%% programmer can manually implement their polling scheme by wrapping 
%% the interface with a loop, such as the {\em waitForEventAndRelease}
%% provide by AMD OpenCL SDK. 

%% The second factor is the process scheduling algorithm in the OS. This
%% factor is especially important when there are some other (CPU)
%% workloads running along with the GPU program on a system. Among the
%% many scheduling algorithms, two are particularly common. The first is
%% the \underline{round-robin} algorithm, in which, tasks with the same
%% priority are given equal CPU time slices in turn, 
%% %tasks with higher
%% %priority can grab time slice from tasks with lower priority, and
%% and the priority of a process does not change during execution. 
%% Recent Windows systems (e.g., Windows 7) have been using such an
%% algorithm. 
%% %It is also one of the options in modern Linux. 
%% The second is the \underline{dynamic priority-based} algorithm. The
%% default algorithm in Linux, {\em SCHED\_OTHER}, belongs to this
%% category. In this algorithm, task priority is adjusted automatically
%% throughout an execution. The dynamic priority is increased for each
%% time quantum in which the process is ready to run but is denied to run
%% by the scheduler~\cite{sched-other}. If the GPU control program stays
%% mostly idle during the execution of a GPU kernel, the dynamic priority
%% of the control program gets raised at the completion of the GPU kernel
%% under this scheduling algorithm, but remains unchanged under the first
%% scheduling algorithm. The higher priority could help the control
%% program attain CPU sooner and hence react more quickly to a kernel
%% completion.

A second factor is the power management in the OS. For instance, if
there are no workload other than the idle GPU control program on
CPU, the OS may put the CPU into sleep.  At the kernel completion, the CPU
has to be waked up before the control program can react; the wake-up
delay adds to the GPU control delay.

%Some variations: dynamic kernel launches, opencl queuing threads, etc.

\subsection{Tensions Among Responsiveness, Energy Efficiency, and Interference}
\label{sec:tensions}

GPU control responsiveness is not well supported by current operating
systems on today's CPU-GPU integrated processors. The fundamental
reason is the tensions among GPU control responsiveness, 
energy efficiency, and co-run interference.

Table~\ref{tbl:tensions} shows the qualitative effects of the two
typical GPU control mechanisms.  Hardware interrupt puts the GPU
control program to sleep while waiting for the kernel to finish execution,
which makes CPU available to other workloads or to sleep. It gives good
energy efficiency and low interference (or called high {\em
 cooperativeness} as in Table~\ref{tbl:tensions}) on other CPU
workloads, but could cause a long GPU control delay. Busy polling
keeps GPU control program occupying CPU during kernel executions; it
helps GPU control responsiveness, but worsens energy efficiency and
cooperativeness. 

\begin{table}
\centering
\caption{Tensions about GPU control responsiveness.\\ ($\downarrow$:
  decrease; $\uparrow$: increase)}\label{tbl:tensions}
\begin{tabular}{rccc}\hline
          & Respons. & Energy eff. & Cooperat. \\
Interrupt: & $\downarrow$ & $\uparrow$ & $\uparrow$ \\
Polling:   & $\uparrow$ & $\downarrow$ & $\downarrow$ \\\hline
%% dyn sch:  & $\downarrow$ & - & $\uparrow$ \\
%% r-r sch:   & $\uparrow$ & - & $\downarrow$ \\\hline\hline
\end{tabular}\\
%% \footnotesize
%% dyn: dynamic priority; r-r: round-robin;
\end{table}

\begin{figure}
\centering
\includegraphics[width=\columnwidth]{figures/Fig_polling.eps}
\caption{Using polling to replace interrupt in Linux brings large
  speedups to GPU programs but seriously deteriorates energy
  efficiency. Platform: AMD Richland / Linux (3.13). 
}\label{fig:polling}
\end{figure}

Figure~\ref{fig:polling} provides the quantitative evidence, collected
on AMD Richland running Linux 3.13. On the set of OpenCL programs
(details in Section~\ref{sec:eval}), busy polling, by avoiding control delays
in the default interrupt-based mechanism, reduces the execution time
of those programs by 10-37\%, but at the same time, increases the
system power consumption by as much as 68\% (measured through power
meter Watts' Up), and degrades co-running CPU programs by as much as
51\% (details in Section~\ref{sec:eval}). The tensions demonstrate the
fundamental dilemma in enhancing GPU control responsiveness. 

%% Modern Linux, by default, uses hardware interrupt and dynamic
%% priority-based scheduling, and puts CPU to sleep if no other jobs are
%% running on CPU during the wait for the kernel completion. It aggressively
%% saves energy, but results in poor cooperativeness and
%% responsiveness. Figure~\ref{fig:coRunLinCPU} shows the poor
%% cooperativeness: The default design makes the GPU program execution
%% degrade the performance of co-running CPU programs by as much as
%% 216\%. (baseline is the performance of standalone executions of those
%% programs.)  Figure~\ref{fig:polling} shows the poor responsiveness:
%% Compared to using busy polling, the default design causes the GPU programs
%% (standalone executions) to run 20\% slower on average.

%% The observed poor support by the default OS is unlikely due to
%% careless designs. Replacing the default mechanisms with existing alternatives
%% does not solve the problem. As Figure~\ref{fig:polling} shows, even
%% though using polling helps boost the responsiveness, it results in
%% 48\% more overall system energy consumption (measured through Watt's
%% Up power meters) on average. The reason is that during the kernel
%% executions, CPU cannot go to sleep anymore, consuming a lot more
%% energy than before.

%% %% Similarly, when we replace the default scheduling policy with
%% %% a round-robin policy, the performance degradation of co-running CPU
%% %% programs reduces from ??X to ??, but the responsiveness of the GPU
%% %% programs (co-run or standalone??)  becomes ?? longer, as shown in
%% %% Figure~\ref{fig:rrLinux}.

%% \begin{figure}
%%   \centering
%%   \includegraphics[width=0.9\columnwidth]{figures/fig_cpusdAPU.eps}
%%   \caption{The average performance degradations of a CPU program when
%%     it co-runs with a GPU program. The programs come from the AMD SDK
%%     benchmark suit.\TODO{Put ``CPU Programs'' as the X-axis label.}}\label{fig:coRunLinCPU}
%% \end{figure}


%% \begin{figure}
%% \centering
%% %\includegraphics[width=0.9\columnwidth]{figures/polling.pdf}
%% \TODO{A bar graph. 2 bars per benchmark: average co-run degradations
%%   of CPU programs compared to their standalone runs; average
%%   performance of GPU programs (co-run or standalone?? compared with
%%   the default runs).}
%% \caption{Using round-robin to replace dynamic priority scheduling in
%%   Linux improves cooperativeness of GPU programs but lengthens 
%%   GPU response delays. Platform: \TODO{machine and
%%     os}.}\label{fig:rrLinux}
%% \end{figure}

%% Windows does not allow kernel changes, but our experiments indicate
%% similar insights. Windows uses round robin scheduling to keep the
%% interference from GPU to CPU low, but causes poor GPU control
%% responsiveness in co-run cases. As Figure~\ref{fig:coRunWin} shows,
%% when some CPU programs share the system, many GPU programs takes more
%% than double the time of their standalone runs to completion. Some detailed
%% analysis we report recently~\cite{Zhu+:LCPC14} shows that the large
%% degradations are not due to memory and cache contention, but due to the
%% GPU control delay: Because of the usage of CPU by the CPU co-runner,
%% the GPU control program has to wait for a longer time before being able
%% to respond to the completion of a GPU kernel. 

%% \begin{figure}
%%   \centering
%%   \includegraphics[width=0.9\columnwidth]{figures/fig_gpu_win.eps}
%%   \caption{The performance degradations of GPU programs on Intel
%%     IvyBridge/Windows when CPU program co-runs on the system
%%     (baseline: the standalone GPU performance.) The bars circled at
%%     the top have degradations over 200\%.}\label{fig:coRunWin}
%% \end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%% detailed co-run performance results on
%%%%%%%%%%%%%%%%%%%%%%%%%%% windows %%%%%%%
%% \begin{figure}[t]
%%   \begin{minipage}[t]{1\linewidth}
%%   \centering
%%   \includegraphics[width=1.0\textwidth]{figures/fig_cpu_win.eps}
%%   \end{minipage}
%%   \begin{minipage}[t]{1\linewidth}
%%   \centering
%%   \includegraphics[width=1.0\textwidth]{figures/fig_gpu_win.eps}
%%   \end{minipage}
%%   \caption{The performance of co-run programs on Intel IvyBridge / Windows. 
%% The programs come from the AMD SDK benchmark suit. The bars circled at the 
%% top have degradations over 200\%, detailed in 
%% Table~\ref{tbl:withFigA}.}\label{fig:coRunWin}
%% \end{figure}

%% \begin{table*}[ht] %table 1
%%   \caption{GPU programs with more than 200\% contention slowdown when
%%     running with CPU contenders}\label{tbl:withFigA}
%%   \centering
%%   \begin{tabular}{|c|c|c|c|c|c|c|c|c|}
%%      \hline
%%      {\bf GPU\_CPU}	&Bino\_AESE	&Blac\_AESE	&Blac\_Mont	&DCT\_AESE	&DCT\_Bino	&DCT\_His	\\
%%      \hline
%%      {\bf Slowdown}	&6.3x		&7.0x 		&2.2x		&20.1x		&4.3x		&10.1x		\\
%%      \hline \hline
%%      {\bf GPU\_CPU}	&DCT\_Sobe	&DCT\_URNG	&Radi\_AESE	&Radi\_Bino	&Radi\_Blac	&Radi\_His	\\
%%      \hline
%%      {\bf Slowdown}	&4.2x		&7.4x 		&39.8x		&5.6x		&2.8x		&9.3x		\\
%%      \hline \hline
%%      {\bf GPU\_CPU}	&Radi\_Sobe	&Radi\_URNG	&Sobe\_AESE	&Sobe\_Mont	&Sobe\_URNG	& - \\
%%      \hline
%%      {\bf Slowdown}	&6.1x		&12.4x		&8.4x 		&2.2x		&2.0x		& - \\
%%      \hline
%%   \end{tabular}
%% \end{table*}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% %==============

%% Recent years have seen a siginificant development of heterogenous system.
%% GPU, which is a hot-star accelorater in the famliy of heterogenous system,
%% has attraced the attention of both industry and academia. It is known to 
%% us that "CPU + accelorater" architecutre can greatly improve porformance. 
%% NVIDIA introduced the concept of GPGPU, and produced the first general 
%% computing GPU. The data transferring between GPU and CPU depends on the 
%% off-chip bus, which has been proved as the performance bottleneck of 
%% the whole system (GPU + CPU). 

%% To address the high latency data transferring overhead, HSA (Heterogeneous 
%% System Architecture) is proposed recently. 
%% The pupose of HSA is to create an improved processor design that 
%% exposes the benefits and capabilities of mainstream programmable compute 
%% elements, working together seamlessly~\cite{HSA}. In this
%% architecture, GPU and CPU share the memory space, so that the transferring 
%% overhead can be totally eliminated. The idea is good, but HSA 
%% still has a long way to go.

%% In modern OS, GPU is managed just like other I/O devices. Except the kernel
%% execution, a typical GPU program also includes some workload executed on CPU,
%% such as initialization, device memory allocation, data transferring and kernel
%% management. A CPU host program is responsible for all these work. In some cases,
%% the CPU usage of GPU program can not be ignored. Therefore, CPU resource 
%% contention exists between a pair of co-run programs on CPU and GPU. And in 
%% our previous study [LCPC], we reveal that in the fused architecture, although 
%% CPU and GPU share the memory space or LLC, the influence of CPU resouce 
%% contention is much larger than the memory contention. 

%% In the co-run scenarios, some interaction phenomenons are due to such resource 
%% contention. To illustrate these phenomenos in a comprehensive way, we evaluate
%% the co-run case on two popular operating systems, Ubuntu 14.04 (Linux kernel 
%% version 3.13) and Microsoft Windows (Win7 sp2). And we also do the measurment on
%% two kinds of popular fused architecture, AMD Richland (A10-6800k) on Linux and 
%% Intel IvyBridge on Windows. 

%% \subsection{Linux Result} 
%% As shown in Figure~\ref{fig:coRunLin}, the first subfigure shows the 
%% significant co-run performance degradation of CPU program, and the second one 
%% reveals that GPU program with CPU contention runs faster than the single-run case. 
%% The baseline of the result in Figure~\ref{fig:clRunLin} is their standalone 
%% performance. The X-axis indicates the names of the target
%% programs and the Y-axis indicates the names of the contenders.

%% \begin{figure}[t]
%%   \begin{minipage}[t]{1\linewidth}
%%   \centering
%%   \includegraphics[width=1.0\textwidth]{figures/fig_cpusdAPU.eps}
%%   \end{minipage}
%%   \begin{minipage}[t]{1\linewidth}
%%   \centering
%%   \includegraphics[width=1.0\textwidth]{figures/fig_gpusdAPU.eps}
%%   \end{minipage}
%%   \caption{The performance of co-run programs on AMD Richland / Linux. The programs come
%% from the AMD SDK benchmark suit. The minus value of y-axile in the figure represents 
%% the preformance speedup. }\label{fig:coRunLin}
%% \end{figure}

%% Brieflly speaking, the first phenomenon is casued by the data transferring operation
%% of GPU program. In
%% %do we have to explain why data copy is neccesarry here? 
%% table~\ref{tbl:datacopy}, we show the performance of CPU program when the contender 
%% (GPU program) with data copy and without data copy. After removing the data copy
%% operation, dramtical performance improvment of CPU program is obtained. Although 
%% transferring data from host memory to device memory can help to improve performance 
%% of GPU program, it can slow down the co-run CPU program.  

%% \begin{table*}[htb]
%%   \caption{Performance degradation caused by HisA (GPU) with and without
%% data copying}\label{tbl:datacopy}
%%   \centering
%%   \begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|}
%%      \hline
%%      {\bf }                             &AESE   &Bino   &Blac   &DCT    &His    &HisA   &Mont   &Radi   &Sobe   &URNG   \\
%%      \hline
%%      {\bf With data copying}            &24\%   &49\%  &47\%    &216\%  &86\%   &32\%   &25\%   &24\%   &103\%  &51\%  \\
%%      \hline
%%      {\bf Without data copying}         &2\%    &3\%   &2\%     &9\%    &9\%    &7\%    &2\%    &18\%   &4\%    &3\%   \\
%%      \hline
%%   \end{tabular}
%% \end{table*}

%% As we mentioned above, in modern OS, GPU is managed just like other I/O devices.
%% The host program depends on interrupt mechanism
%% to recognize the completion of a GPU kernel. The interrupt scheme suggests 
%% possible delays for the host program to realize the completion of a GPU kernel
%% when the CPU core is idle to save power. Due to the current CPU power 
%% managment, when the host program waits for the completion interrupt of GPU 
%% kernel, the occupied CPU core goes to sleep to save power (if there is no 
%% another contender task). At the moment of interrupt arriving, CPU core should 
%% firstly be waked up to execute the workload of GPU host program. Considering the 
%% short running time of fast GPU program, this wakeup latency is not trival. 

%% On the other side, if GPU program is running with contender, the co-run CPU 
%% program can prevent the CPU core asleep. Therefore, the workup latency do not 
%% exist any more in the co-run case. 

%% To confirm the interpretation, we remeasure the performance of GPU program with
%% CPU contention. But in this case, CPU sleep mangement is disable so that CPU cores
%% are always active. As shown in Figure~\ref{fig:Dsleep}, performance degradation 
%% and improvment of GPU program are both trival, which are less than 5\%. This 
%% indicates that without influnce of CPU sleep management and wakeup latency, the 
%% performance of single-run GPU program and that in co-run case are similar. The 
%% shortage of accelerator-aware mangement of current OS is the main reason for the 
%% unexpected co-run speedup of GPU program. 

%% \begin{figure}[t] %figure 9
%%   \centering
%%   \includegraphics[width=\columnwidth]{figures/fig_gpudegdsleep.eps}
%%   \caption{Performance degradation of GPU programs with CPU contention. 
%% The sleep state of CPU is disabled. (measured by host timer).}\label{fig:Dsleep}
%% \end{figure} 

%% \subsection{Windows Result}
%% Figure~\ref{fig:coRunWin} shows the
%% performance degradation of CPU programs when the contender runs on the
%% GPU\@. The baseline is their standalone performance on the CPU. 
%% The X-axis indicates the names of the target
%% programs and the Y-axis indicates the names of the contenders. The
%% results show that most CPU programs are subject to minor performance
%% degradations: 88 out of the 100 co-runs have degradation less than
%% 5\%. Program \emph{DCT}, when co-running with \emph{DCT} on the GPU,
%% exhibits the largest degradation, 14\%.

%% In comparison, the degradations of GPU programs are much more
%% significant. Compared to their standalone performance on the GPU, 
%% the programs exhibit dramatic
%% performance degradations: 46 out of the 100 co-runs degrade over 50\%,
%% up to a factor of 40 times slowdown. Meanwhile, we observed speedups
%% on 14 co-runs ranging from 1\% to 12\%.


%% Comparing the result of Linux, we can find that co-run GPU program suffers much
%% more degradation on Windows. This is due to the task scheduling scheme. 
%% Documents show that the scheduler in Windows is
%% largely round-robin among tasks of the same priority. On the other
%% hand, Linux by default (SCHED\_OTHER) uses a dynamic priority scheme
%% in its scheduling. It also has a static priority concept. Each process
%% has a static priority assigned at the creation time. However, during
%% the executions of the processes, the system automatically computes the
%% dynamic priority of each process at each scheduling time point. 

%% Consider two processes of the same static priority, processes A and
%% B\@. Suppose that A was blocked most of the time in the previous time
%% interval, while B was seldom blocked.  A is likely to get a higher
%% dynamic priority than B does.  As a result, after process A finally
%% becomes runnable, the system typically puts it to run immediately and
%% also assigns longer time slices to it than to process B in the next
%% time interval.  In the co-run case, the process of the GPU program is
%% like process A\@. It gets blocked most of the time during the
%% execution of the GPU kernel. The CPU program is like process B\@. The
%% Linux scheduler hence effectively gives a higher priority to the GPU
%% host program than the Windows scheduler gives, and a lower priority to
%% the CPU program than the Windows scheduler gives. The consequence is
%% that the CPU program suffers a larger co-run degradation in Linux than
%% in Windows for its lower priority in Linux, while the GPU program
%% suffers much less delay in its realization of the completion of the
%% GPU kernels in Linux and hence smaller co-run degradations than in
%% Windows. 

%% Preserving CPU core for the host GPU program can eliminate 
%% performance influence caused by CPU resource contention. But this 
%% approach is not good for two reasons. First is that the computation 
%% power of the preserved core can be wasted if the GPU host program does
%% not occupy too much CPU resouce. The second reason is that in the
%% one-CPU-multi-accelerator case, preserving one core for each accelerator
%% (such as GPU, DSP, FGA and so on) can consume CPU resouce dramatically. 

%% In this section, several performance and power management issues are revealed,
%% including kernel completion detecting, CPU resource contention (data 
%% copy and context switch) and OS task scheduling. They all affect
%% program exection but none of them has been well studied in the GPU+CPU
%% programs corun scenario. 

%% [first give some background on GPU execution, including the usage of
%%   CPU by a GPU program]

%% [observations of the influence of OS power management and the
%%   contention of CPU]
