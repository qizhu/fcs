\section{Introduction}

We are witnessing the emergence of integrated CPU-GPU architectures such as
the AMD Fusion, Intel Ivy Bridge, and NVIDIA Denver.
As CPU and GPU are integrated into a single chip and share
the memory system, the communication latency between them is greatly
shortened.  An important consequence is that now short computation
kernels (a few hundred microseconds or less) may effectively utilize
GPU acceleration.
These kernels, although being short, are often invoked repeatedly and
weigh much in the overall execution. Figure~\ref{fig:loop} illustrates
the structure of a breadth-first search (BFS) kernel, which repeatedly
invokes a GPU kernel to mark each node in a graph its BFS level. 
%One call to the kernel takes only several hundreds of microseconds to run,
%but its accumulative time weighs most of the entire program execution. 
Such a structure exists in many other GPU programs,
including iterative algorithms such as Floyd--Warshall
algorithm, clustering algorithm, reduction and so on.  

\begin{figure}[t]
01: int i=0;\\
02: while (!Finished)\{\\
03:\hspace*{.1in}...\\
04:\hspace*{.1in}//Launch the GPU kernel to mark level i nodes\\
05:\hspace*{.1in}BFS\_kernel\_gpu();\\
06:\hspace*{.1in}i++;\\
07:\hspace*{.1in}...\\
08:\}
\caption{Pseudo code of a Breath-First Search (BFS) program.}\label{fig:loop}
\end{figure}

%%   As
%% a result of the repeated invocations, acceleration of these short
%% kernels by GPU can often give large improvement to the overall
%% performance of these applications~\cite{harris2007optimizing,
%%   harish2007accelerating, Bakhoda+:ISPASS09}.

Despite the reduction of communication latency, the GPU acceleration
of short GPU kernels is still hampered by poor control responsiveness,
which refers to how quickly the host program reacts to the completion of 
a GPU kernel.
%Conceptually, GPU control follows a common design: A program execution
%starts with a CPU thread; that thread or its child thread launches a
%GPU kernel; when the GPU kernel finishes execution, the launching thread finds
%that out through a certain mechanism (e.g., hardware interrupt issued
%by GPU, polling a special register); it then continues the execution
%of the program, and possibly makes other GPU kernel calls. We call the
%launching thread a {\em GPU control thread} or simply a {\em control
%thread}. The delay for the control thread to find out the completion
%of a GPU kernel is the so-called GPU control delay.
GPU control delay is substantial on today's CPU-GPU integrated systems,
reaching as much as 100 microseconds on our AMD Richland APU running Linux.
%; on Intel ?? running Windows, it is as much
%as ??. The delay becomes even larger---as much as ??---when some other
%programs are running on the CPU while the GPU program executes. 
%For
%programs with short (but repeatedly invoked) GPU kernels, GPU control
%delay dramatically throttles the benefits of using GPU for
%accelerations. As Figure~\ref{fig:potential} shows, removing such
%delays can potentially bring 7--29\% speedup and 12--50\% power 
%efficiency improvement on a set of OpenCL
%benchmarks on the AMD processor.

%\begin{figure}[t]
%\centering
%\includegraphics[width=\columnwidth]{figures/Evaluation/fig_potential.eps}  
%\caption{Potential of performance and power efficiency without control delay.}\label{fig:potential}
%\end{figure}

%% \begin{table}
%% \centering
%% \caption{Speedups obtained when GPU control delays are minimized (by
%%   letting control threads keep checking the status of the kernel,
%%   detailed in Section~\ref{sec:back}). Kernel Length represests the
%%   average kernel running time in one call.\TODO{replace the length
%%     with miliseconds; use the kernel length when there is no GPU
%%     control delay.}}\label{tbl:busywait}
%% \small
%% \begin{tabular}{|c|>{\centering}m{0.8in}|c|}\hline
%% Benchmark 		& Kernel Length (in seconds) 	& Speedup \\
%% \hline
%% AtomicCounters		& 0.000052	& 15\% 	\\
%% \hline
%% BinomialOption		& 0.001988	& 7\%	\\
%% \hline
%% BitonicSort		& 0.000072	& 25\%	\\
%% \hline
%% BlackScholes		& 0.000363	& 14\%	\\
%% \hline
%% DCT			& 0.000100	& 14\%	\\
%% \hline
%% DwtHaar1D		& 0.000081	& 36\%	\\
%% \hline
%% FastWalshTransform	& 0.000053	& 36\%	\\
%% \hline
%% FloydWarshall		& 0.000083	& 25\%	\\
%% \hline
%% Histogram		& 0.000520	& 11\%	\\
%% \hline
%% MatrixMulImage		& 0.000145	& 13\%	\\
%% \hline
%% MatrixMultipl		& 0.000108	& 15\%	\\
%% \hline
%% MatrixTranspose		& 0.000083	& 23\%	\\
%% \hline
%% QuasiRandomSequence	& 0.000114	& 28\%	\\
%% \hline
%% Reduction		& 0.000056	& 14\%	\\
%% \hline
%% SimpleConvolution	& 0.000061	& 25\%	\\
%% \hline
%% StringSearch		& 0.002523	& 6\%	\\
%% \hline
%% \end{tabular}
%% \end{table}

The control delay can be attributed to several reasons, including
the cost of interrupt handling and the exit latency of CPU hardware
sleeps (known by some as the ``C'' states).
%Several reasons contribute to the GPU control delays. They are of the
%designs of either the GPU driver or the Operating Systems (OS),
%including the mechanism used to notify CPU the completion of a GPU kernel,
%CPU power management, and process scheduling.  We will detail these
%reasons in the coming section. Here, it is worth noting that even
%though it may be possible to reduce the control delay by changing OS
%or GPU drivers, the changes often cause negative effects on other
%aspects. For instance, on AMD Richland systems running Linux (kernel
%version 3.13), hardware interrupt is used to notify the control thread
%of a kernel's completion, which causes long control delays. 
If the interrupt is replaced with busy waiting (i.e., the host program
continuously polls the GPU kernel completion status), the delay can be 
minimized.  However, in that
scheme, the CPU would waste lots of CPU cycles polling during the
kernel execution, causing poor energy efficiency (as much as 40\% less
efficient on our set of OpenCL benchmarks). Energy efficiency is
especially important for the integrated processors, because for their
smaller size, they may also be employed on portable devices where
battery life is critical. Moreover, when there are other CPU programs
sharing the system, the busy polling would cause serious interference
to co-running programs.


%% One direction to improve GPU control responsiveness is to enhance the
%% underlying operating sytems and drivers. There are two factors in the
%% systems that affect GPU control responsiveness. The first is the
%% scheme for notifying the GPU control thread the completion of a GPU kernel
%% execution. The scheme is part of the GPU driver. There are two primary
%% schemes: hardware interrupt and register busy-polling. Unfortunately,
%% they both suffer from the tension between responsiveness and energy
%% efficiency: Hardware interrupt saves CPU energy, but causes large
%% response delay, while busy-polling minimizes response delay, but
%% wastes CPU energy. 

%% The second factor is the process scheduling policy in operating
%% systems (OS).  There are two main scheduling policies in mainstream
%% systems: dynamic priority-based scheduling and round-robin
%% scheduling. They both suffer from a tension between GPU responsiveness
%% and interferences to other programs running on the CPU (called CPU
%% co-runners). Dynamic priority-based scheduling (e.g., the default
%% policy in Linux) reduces the response delay of GPU but causes large
%% interference to CPU co-runners, while round-robin scheduling (e.g.,
%% the default policy in Windows 7) reduces the interference on CPU, but
%% causes poor GPU control responsiveness. We further elaborate the
%% dilemma in Section~\ref{sec:problem}.

%\paragraph{Overview of This Work}

This discussion illustrates a fundamental dilemma between GPU
responsiveness, energy efficiency, and co-run interference.  Such a
dilemma exists in traditional I/O device (e.g., disk). However,
several properties of integrated GPU make the issue special.

{\em (1) Short length.} As mentioned earlier, the tight
 CPU-GPU integration avoids cross-device data transfers and thus
 allows fine-grained tasks to take advantage of GPU acceleration.
 For instance, the shortest kernel in our benchmarks takes only 58
 microseconds to run (while its repeated invocations make it weigh
 significant in the program overall execution time).
 %(In comparison, an access to a hard drive typically takes ??.)

{\em (2) Computing engine.} As GPU serves as a main computing engine
rather than peripheral devices, GPU control delays often lengthen the
critical path. This property, coupled with the first property, entails
that even short delays in GPU control could drastically reduce the
benefits of GPU acceleration on such integrated processors.

{\em (3) Large variations.} As GPU is a tuning-complete computing
device, GPU kernel lengths have a large, potentially infinite, range
of variations. The operations of a traditional peripheral device may
fluctuate, but in a much limited range. GPU kernels are different:
Some finish execution in several hundreds microseconds, while some
could run for seconds or even minutes.  Moreover, different iterations
of a kernel can also exhibit dramatic variations in length, due to the
changes in the input data sets. 
%For example, the running time of a kernel in
%BlackScholes from AMD SDK fluctuates by as much as 25\% of the average
%length across repeated runs of the same kernel.  
Predicting GPU kernel lengths is hence especially
challenging. Consequently, traditional ways to treat control
delay---designing the control based on a typical length of the
operations---cannot suite GPU well (as Section~\ref{sec:eval} shows).

This paper introduces the first program-level solution to this
problem.  Through a compiler-based code transformation, we inject into
a GPU program the capability to respond to GPU kernel completions
promptly with high energy efficiency. As a program-level solution (in
contrast to OS techniques~\cite{Zhu+:HotOS15}), it is
\underline{resilient} to OS designs and is \underline{portable} across
different generations of GPUs.  Moreover, it proves to be
\underline{general}, working effectively on both regular and irregular
programs, and on GPU executions with or without co-running CPU
programs.

The core of the solution is a technique we call {\em anticipatory
  wakeup}, the basic idea of which is simple and intuitive: If we can
make the host program check the status of the GPU kernel
right in time, it could avoid the long control delay without wasting
many CPU cycles as busy polling does. The challenge is on how to
enable the ``right in time'' check---it is difficult to predict the length
of a kernel accurately; at the same time, a small disparity
could degrade GPU program performance significantly due to the high
sensitivity of the short kernels on control delays.

We conduct a
systematic exploration in the design space of anticipatory wakeup. The
exploration considers a set of techniques and compares a series of
designs on both regular and irregular programs. The result shows that
two novel techniques are especially effective. The first is {\em
  self-correction}, a method that automatically rectifies wrong
predictions of GPU kernel lengths. It helps circumvent the difficulty
in accurate kernel length prediction. The second technique is {\em
  Splitting-based Anticipatory Wakeup (SAW)}, which, through splitting
a GPU kernel into two, helps mitigate the negative effects of errors
in kernel length predictions. Together, the two techniques make
anticipatory wakeup an effective approach to avoid much control delay
without wasting many CPU cycles.

We show that the anticipatory wakeup can be easily enabled for a GPU
program through code transformations by a source-to-source
compiler. Besides injecting the capability into a program, the
compiler also helps automatically select the design of anticipatory
wakeup that best suits a given program. 

%% The second is to minimize the amount of CPU activities required by the
%% GPU control thread, which may help reduce interference and energy
%% consumption, and benefit control responsiveness at the same time.  We
%% explore two simple code transformations for that purpose. They are
%% {\em GPU-initiated data copy} and {\em kernel fusion}; one minimizes
%% data copy work by CPU, the other minimizes needed kernel control
%% activities. 

Experimental results on an AMD Richland APU show that the optimization 
improves
the performance of a set of GPU benchmarks by as much as 21\% (12\% on
average), and meanwhile, increase the energy efficiency (energy delay
product or EDP) by 11\% on average, relative to the default executions
of the programs. 

Moreover, experiments on another generation of GPU
(AMD Kaveri) show similar benefits, confirming the portability of the
program-level solution. However, due to the CPU-GPU coupled power gating, 
larger launching overhead of GPU kernel, and shared virtual memory, the 
issue on Kaveri GPU is more complicated. In our future study, we will 
focus on understanding and handling these subtle effects.

%% Our experiments show that the optimizations working together can
%% significantly improve GPU performance, energy efficiency, and at the
%% same time, reduce the interferences of GPU programs on co-running CPU
%% programs. We test them on two types of integrated
%% CPU-GPU hardware (Intel IvyBridge, AMD Fusion) that are equipped with
%% two different drivers and operating systems (Windows 7 and Linx). The
%% results show that \TODO{to fill after the evaluation section is done}.

%% We
%% test the solution on standalone executions of a set of OpenCL
%% programs, as well as their co-runs with CPU workloads. The result show
%% that in single-run case, {\em in-time wakeup} can eliminate control
%% delay with a low power level, which speedups as much as 36\%
%% performance. And it also improves as much as 30\% power efficiency
%% compared with busy waiting. In co-run case, both kernel fusion and
%% GPU-initiated data copy show significant affect to improve co-run
%% performance. The speedup of GPU program is as much as 20x without
%% performance loss of the co-run CPU program.

\paragraph{Contributions}
Overall, this work makes the following major contributions:

\begin{itemize}
\item To our best knowledge, this paper presents the first
  program-level solution for resolving the GPU responsiveness dilemma---%
  realizing fast responses, high energy efficiency, and low CPU costs 
  at the same time.

\item It proposes the technique of {\em self-correction} for
  circumventing the difficulties of accurately predicting GPU kernel
  lengths.

\item It introduces the concept of {\em splitting-based anticipatory
  wakeup}, which reduces the effects of wrong kernel length
  predictions especially on complex, irregular GPU programs.

\item It gives a systematic comparison of various designs of
  anticipatory wakeup for GPU control, and presents a set of insights
  in designing and automatically selecting the suitable design for a
  given GPU program.
\end{itemize}

In the rest of this paper, we first provide some background on GPU
control and the responsiveness dilemma in Section~\ref{sec:back}. We
then describe the proposed anticipatory wakeup techniques in
Section~\ref{sec:aw}. We discuss design selection and compiler
implementations in Section~\ref{sec:impl}. We report the experimental results
in Section~\ref{sec:eval}, and conclude the paper with some related
work discussions in Section~\ref{sec:rel} and a summary in Section~\ref{sec:con}.

